#!/bin/bash
echo "-----------------start-----------------"
if [ -n "$1" ]; then
    java -cp $1 io.transwarp.test.TestDate
elif [ -f '/root/bin/test-date-1.0.0.jar' ]; then
    java -cp /root/bin/test-date-1.0.0.jar io.transwarp.test.TestDate
elif [ -f '/root/de_tools_工具集/test-date-1.0.0.jar' ]; then
    java -cp /root/de_tools_工具集/test-date-1.0.0.jar io.transwarp.test.TestDate
else
    java -cp test-date-1.0.0.jar io.transwarp.test.TestDate
fi


# java -cp /root/test-date-1.0.0.jar io.transwarp.test.TestDate "2017-09-18"
echo "-----------------finish-----------------"

